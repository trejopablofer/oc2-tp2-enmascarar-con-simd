# OC2 TP2 - Enmascarar con SIMD

- [OC2 TP2 - Enmascarar con SIMD](#oc2-tp2---enmascarar-con-simd)
  - [Implementación](#implementación)
    - [Enmascarar en C](#enmascarar-en-c)
    - [Enmascarar en ASM con SIMD](#enmascarar-en-asm-con-simd)
  - [Conclusiones](#conclusiones)



Trabajo Práctico n°2 de Organización del Computador 2

En este documento se describirá la resolución del enunciado del TP2. Éste consistió en el desarrollo de una aplicación que combine dos imágenes en una utilizando una máscara de selección. Este proceso debió ser implementado de dos formas diferentes:
- En lenguaje C
- En lenguaje ensamblador

El objetivo de hacer la misma operación en dos lenguajes distintos fue el de comparar el rendimiento de las instrucciones SIMD (Single Instruction - Multiple Data) en lenguaje ensamblador frente a las instrucciones aritmético-lógicas de la ALU en lenguaje C.

## Implementación
La idea de la implementación giró en torno a que la aplicación reciba tres imágenes en formato .bmp de mismas dimensiones (2 fotos y una máscara) y un número de ancho y alto como parámetros. Luego tiene que devolver una imagen resultado de llenar los píxeles en blanco de la máscara con los píxeles de la 1ra imagen, y los píxeles en negro con los de la 2da. He aquí un ejemplo:

**Imagen 1**

<p align="center"><img src="rpics/1.png"/></p>

**Imagen 2**

<p align="center"><img src="rpics/2.png"/></p>

**Máscara**

<p align="center"><img src="rpics/mask.png"/></p>

**Resultado de la combinación**

<p align="center"><img src="rpics/result.png"/></p>

Nota: A diferencia de lo enunciado, se optó por trabajar con imágenes en formato .bmp en lugar de .rgb debido a que tuvimos varias complicaciones con este último.

En primer lugar, se chequea que los parámetros de entrada al ejecutar sean la cantidad correcta (nombre del ejecutable, nombre de los tres archivos a usar, ancho y alto).
<p align="center"><img src="rpics/param.png"/></p>

Para asegurarnos de que el formato de los parámetros recibidos (archivos y medidas) sea el correcto, creamos las siguientes funciones:

-*verificarNumeros*: verifica que el ancho y el alto del archivo sea un número.
<p align="center"><img src="rpics/verNum.png"/></p>

-*verificarBMP*: verifica que la extensión del archivo sea ".bmp".
<p align="center"><img src="rpics/verBMP.png"/></p>

-*verificarArchivo*: chequea que la ruta del archivo especificado exista.
<p align="center"><img src="rpics/verArch.png"/></p>

-*buscar_en_carpeta*: arma el nombre real del archivo especificado agregándole la resolución para poder encontrarla (por ejemplo, al ingresar "imagen.bmp" la transforma en "640x480.imagen.bmp"). Es una forma de no tener que ingresar un nombre de imagen tan largo en cada ejecución.
<p align="center"><img src="rpics/buscar.png"/></p>

Con las funciones *verificarNumeros* y *verificarBMP* se valida el formato de los parámetros de entrada: las dos imágenes a combinar, la máscara y el ancho y alto de todas ellas.
<p align="center"><img src="rpics/validar.png"/></p>

Luego con *buscar_en_carpeta* se construye el nombre real de los archivos a ejecutar, cuyo formato luego se valida con *verificarArchivo*.
<p align="center"><img src="rpics/validar2.png"/></p>

Teniendo los valores validados, ya es posible realizar el enmascarado de las imágenes, el cual se efectúa a través de las funciones **enmascarar_c** y **enmascarar_asm**, que serán detalladas a continuación. En esta parte se ejecutan las funciones y se imprimen los resultados de tiempo de cada una.
<p align="center"><img src="rpics/llamado.png"/></p>

### Enmascarar en C
La función **enmascarar_c** recibe los nombres de las imágenes y máscara a procesar junto con su tamaño. 
<p align="center"><img src="rpics/c.png"/></p>

Se inicializa el reloj para medir el tiempo de ejecución (en pos de comparar la performance más adelante) y se inicializan el tamaño del buffer negro, buffer blanco y el puntero a utilizar.
<p align="center"><img src="rpics/st.png"/></p>

Luego se hace una asignación dinámica de memoria con la función *malloc* para reservar tamaño para las variables, y se abren los archivos.
<p align="center"><img src="rpics/malloc.png"/></p>

Ya con los archivos abiertos, se procede a leer cada uno y a escribir sobre el archivo de salida según lo que se encuentre en el primer parámetro de la lectura de la máscara (pointer). Si éste vale 0 (es un píxel de color negro), escribe lo que valga la imagen 1 sobre esa posición en el archivo de salida. Caso contrario (píxel blanco), escribe lo que valga la imagen 2 sobre la misma. Una vez que se termine de leer la máscara, se termina el ciclo y queda la imagen de salida completa.
<p align="center"><img src="rpics/while.png"/></p>

Por último se cierran los archivos utilizados y se libera la memoria asignada al hacer los *malloc*. También se detiene el reloj y se retorna la cantidad de ticks que duró la función.
<p align="center"><img src="rpics/close.png"/></p>

### Enmascarar en ASM con SIMD
La función **enmascarar_asm** también recibe los nombres de las imágenes y máscara a procesar junto con su tamaño.
<p align="center"><img src="rpics/asm.png"/></p>

Se inicializa el reloj para medir el tiempo de ejecución (en pos de comparar la performance más adelante)
y los buffer correspondientes a la imagen 1 y 2, máscara y salida, reservándoles espacio en memoria con *malloc* otra vez.
<p align="center"><img src="rpics/malloc2.png"/></p>

Luego se llenan los buffer de las imágenes y máscara con los datos utilizando la función *llenar_buffer*.
<p align="center"><img src="rpics/lb.png"/></p>

Esta función abre un archivo, lo lee y lo cierra. Esto genera una lectura de cada archivo y guarda cada elemento leído en el buffer pasado por parámetro.
<p align="center"><img src="rpics/lbfunc.png"/></p>

Aquí subyace la diferencia principal con *enmascarar_c*. En este caso tomamos los buffer generados de los archivos y llamamos a la función externa *solver_asm* escrita en lenguaje ensamblador para que haga el trabajo del enmascarado en lugar del programa en C.
<p align="center"><img src="rpics/solver.png"/></p>

Lo primero hecho fue declarar tres variables auxiliares que serán explicadas cuando sean utilizadas. 
<p align="center"><img src="rpics/var.png"/></p>

*solver_asm* es como se llama a la función desde C. Las primeras dos líneas limpian el registro ebx y ecx.
Luego se recupera el dato del tamaño de los archivos en la línea 14.
<p align="center"><img src="rpics/solverasm.png"/></p>

En la etiqueta _ciclo_ se recorren los archivos pasados de a 8 bytes y se ingresan a los registros mm0 (imagen 1), mm1 (imagen 2) y mm2 (máscara).
<p align="center"><img src="rpics/ciclo.png"/></p>

<p align="center"><img src="rpics/lineas.png"/></p>

* En la linea 28 se llena el registro mm3 con '1' para luego utilizarla con la máscara para negarla. Es decir, lo negro se vuelve blanco y lo blanco, negro.

* En la linea 29 se combinan la imagen 2 y la máscara. Los bytes que estén en la posición donde es negro en la máscara pasan a ser negros (0).

* En la linea 30 se hace un xor entre la máscara y el vector de '1' para dar vuelta la máscara, es decir, negarla.

* En la linea 31 se combinan la imagen 1 y la máscara negada. Los bytes que estén en la posición donde es negro en la máscara pasan a ser negros (0).

* En la linea 34 se juntan las dos imágenes y el resultado se queda en la imagen 1.

* En la linea 36 se persiste el resultado de esta iteración y se sigue con otros 8 bytes.

En la etiqueta _ultimo_loop_ se recorren los últimos bytes de los archivos, ya que la cantidad de bytes de los archivos ingresados no siempre van a ser múltiplos de 8. Por este motivo se decidió realizar la ejecución de esta última parte de los archivos pero traduciendo las instrucciones SIMD del ciclo pasado a instrucciones tradicionales del lenguaje ensamblador.
<p align="center"><img src="rpics/ultimoloop.png"/></p>

Ésta sería todo el código de la función *solver_asm*.

Al terminar la función se obtiene un buffer de salida (el buffer de la imagen 1 editado con la imagen 2 superpuesta), con el cual se genera un archivo BMP con la función *generarBMP*. Esta función abre el archivo de salida, le escribe el buffer pasado por parámetro y lo cierra. 
<p align="center"><img src="rpics/gbmp.png"/></p>
<p align="center"><img src="rpics/gbmps.png"/></p>

Por último se libera la memoria asignada a los buffer de las imágenes y máscara. También se detiene el reloj y se retorna la cantidad de ticks que duró la función.
<p align="center"><img src="rpics/free.png"/></p>

## Conclusiones
Algunos de los inconvenientes encontrados en el desarrollo del trabajo fueron los siguientes:
- Hubo que pasar de utilizar imágenes en formato RGB a BMP debido a problemas de visualización.
- Se tuvieron muchos problemas en la parte del lenguaje ensamblador, sobre todo, las segmentation faults que no se pudieron arreglar y que impidieron el enmascarado en lenguaje assembler y, por tanto, la comparación de performance entre ambos métodos.

Como aprendizaje interno, nos quedamos con que la gestión de memoria es más complicada de lo que parece, y de su importancia a la hora de compilar un programa.
