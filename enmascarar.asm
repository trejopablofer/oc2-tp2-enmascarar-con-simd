section .data
    imagen1 db 0
    imagen2 db 0
    mascara db 0

section	.text

global solver_asm

solver_asm:
    xor ebx, ebx
	xor ecx,ecx

    mov ebx, [ebp + 20]     ;Tamaño de los archivos
    mov ecx, 0
    
    ciclo: 
        cmp ebx,8
        jnae ultimo_loop
          
        mov edx, [ebp + 8]
        movq mm0, qword[edx+ecx] ;imagen1 
        mov eax, [ebp+12]
        movq mm1, qword[eax+ecx]  ;imagen2
        mov eax, [ebp+16]
        movq mm2, qword[eax+ecx]  ;mascara

        pcmpeqb mm3, mm3
        pand mm1, mm2
        pxor mm2, mm3 ;lo negro es blanco y lo blanco es negro("Complemento de la máscara")
        pand mm0, mm2 ;En la imagen 1 si el pixel esta en el lugar de un blanco 
        ;(mask original) (negro en mask negada) se reemplaza y le queda el negro (0)
        ;para que lo pueda usar el otro vector
        por mm0, mm1 ;[1010110] [0011001]

        movq qword[edx + ecx], mm0
        sub ebx, 8
        add ecx, 8
    jmp ciclo

    ultimo_loop:
        cmp ebx,0
        je fin
        
        mov eax, [ebp+12]
        mov edx,[eax+ecx]  
        mov [imagen2], edx ;imagen2
        mov eax, [ebp+16]
        mov edx,[eax+ecx] 
        mov [mascara], edx  ;mascara

        ;pand mm1, mm2
        mov eax, [imagen2]
        mov edx, [mascara]
        and eax, edx
        mov [imagen2], eax

        mov eax, [ebp+16]
        mov edx,[eax+ecx] 
        mov [mascara], edx
        ;pcmpeqb mm3, mm3
        xor dword[mascara], 255
        ;pand mm0, mm2
        mov edx, [ebp + 8]
        mov eax, [edx+ecx] 
        and eax, [mascara]
        ;por mm0, mm1 ;Juntamos las imágenes
        mov edx, [imagen2]
        or eax,edx
        mov [imagen1], eax

        add ecx, 1
        sub ebx, 1
        jmp ultimo_loop
        
fin:
    ret
