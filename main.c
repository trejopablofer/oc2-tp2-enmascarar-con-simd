#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mascara, int width, int height);

int enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mascara, int weigth, int heigth);
extern void solver_asm(unsigned char *img1, unsigned char *img2, unsigned char *mascara, int cantCaracteres);
int llenar_buffer(char *archivo, unsigned char *buffer, int cantidad);
int generar_BMP_salida(char *archivo, unsigned char *buffer, int cantidad);
int obtenerCantCaracteresDeMas(int weigth, int heigth);
char *imagen_ret_asm = "return_asm.bmp";

int verificarNumeros(int largo, char *argv[], int num);
int verificarBMP_RGB(int largo, char *argv[], int num);
void buscar_en_carpeta(char *ancho, char *alto, char *imageFile1, char pathImg1[150]);
int verificarArchivo(char *image);
char *imagen_ret_c = "return_c.bmp";

int main(int argc, char *argv[])
{
    if (argc < 6)
    {
        printf("Faltan parámetros\n");
        return -1;
    }
    else if (argc > 6)
    {
        printf("Hay parámetros de más\n");
        return -1;
    }
    else
    {
        
        //validar imagen 1
        int largo = strlen(argv[1]);
        int checker = verificarBMP_RGB(largo, argv, 1);

        //validar imagen 2
        largo = strlen(argv[2]);
        checker += verificarBMP_RGB(largo, argv, 2);

        //validar mascara
        largo = strlen(argv[3]);
        checker += verificarBMP_RGB(largo, argv, 3);

        //validar ancho
        largo = strlen(argv[4]);
        checker += verificarNumeros(largo, argv, 4);

        //validar alto
        largo = strlen(argv[5]);
        checker += verificarNumeros(largo, argv, 5);

        if (checker != 0)
        {
            return -1;
        }
        
        char *img_1 = argv[1];
        char *img_2 = argv[2];
        char *mascara = argv[3];
        char *ancho = argv[4];
        char *alto = argv[5];

        char dir_img_1[150] = "imagenes//";
        buscar_en_carpeta(ancho, alto, img_1, dir_img_1);
        img_1 = dir_img_1;

        char dir_img_2[150] = "imagenes//";
        buscar_en_carpeta(ancho, alto, img_2, dir_img_2);
        img_2 = dir_img_2;

        char dir_mascara[150] = "imagenes//";
        buscar_en_carpeta(ancho, alto, mascara, dir_mascara);
        mascara = dir_mascara;

        /*checker += verificarArchivo(img_1) + verificarArchivo(img_2) + verificarArchivo(mascara);

        if (checker != 0)
        {
            return -1;
        }
        */

        int width = atoi(ancho);
        int height = atoi(alto);
        int duracion_C = enmascarar_c(img_1, img_2, mascara, width, height);
        //int duracion_asm = enmascarar_asm(img_1, img_2, mascara, width, height);
        printf("Combinación exitosa :D\n");
        printf("Revise el archivo de salida en la carpeta principal\n");
        printf("Duración de la ejecución con C: %d ticks\n", duracion_C);

        // printf("Duración de la ejecución con Assembler: %d", duracion_asm);
        /*
       if(duracion_C > duracion_asm){
           printf("Para el tamaño %s x %s de las imágenes, C es más rápido\n", argv[4], argv[5]);
       }else if (duracion_asm > duracion_C){
           printf("Para el tamaño %s x %s de las imágenes, C es más rápido\n", argv[4], argv[5]);
       }else{
           printf("Ambas ejecuciones tardaron lo mismo\n");
       }
       */
        return 0;
    }
}

int enmascarar_c(unsigned char *imagen_a, unsigned char *imagen_b, unsigned char *mascara, int width, int height)
{
    clock_t inicio, fin;
    inicio = clock();

    size_t st_ptr = 1;
    size_t st_buffer_negro = 1;
    size_t st_buffer_blanco = 1;

    //reservo tamaño para mis variables
    char *pointer = malloc(st_ptr);
    char *buffer_negro = malloc(st_buffer_blanco);
    char *buffer_blanco = malloc(st_buffer_negro);

    FILE *arch_img_1 = fopen(imagen_a, "rb");
    FILE *arch_img_2 = fopen(imagen_b, "rb");
    FILE *arch_mascara = fopen(mascara, "rb");
    FILE *salida = fopen(imagen_ret_c, "wb");

    //feof -> end of file
    while (!feof(arch_mascara))
    {
        fread(pointer, 1, st_ptr, arch_mascara);
        fread(buffer_blanco, 1, st_buffer_blanco, arch_img_1);
        fread(buffer_negro, 1, st_buffer_negro, arch_img_2);

        if (*pointer == 0)
        {
            fwrite(buffer_negro, 1, st_buffer_negro, salida);
        }
        else
        {
            fwrite(buffer_blanco, 1, st_buffer_blanco, salida);
        }
    }

    fclose(arch_img_1);
    fclose(arch_img_2);
    fclose(arch_mascara);
    fclose(salida);
    free(pointer);
    free(buffer_negro);
    free(buffer_blanco);

    fin = clock();

    return fin - inicio;
}
/*
###################################################################################################################################
*/

int enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mascara, int weigth, int heigth)
{
    clock_t inicio, fin;
    inicio = clock();

    int longitud_archivos = weigth * heigth * 3;

    unsigned char *buffer_imagen_1 = (unsigned char *)malloc(sizeof(unsigned char) * longitud_archivos);
    unsigned char *buffer_Imagen_2 = (unsigned char *)malloc(sizeof(unsigned char) * longitud_archivos);
    unsigned char *buffer_mask = (unsigned char *)malloc(sizeof(unsigned char) * longitud_archivos);

    llenar_buffer(a, buffer_imagen_1, longitud_archivos);
    llenar_buffer(b, buffer_Imagen_2, longitud_archivos);
    llenar_buffer(mascara, buffer_mask, longitud_archivos);

    solver_asm(buffer_imagen_1, buffer_Imagen_2, buffer_mask, longitud_archivos);
    printf("Terminó ejecucion en ASM CONGRATS!\n");

    generar_BMP_salida(imagen_ret_asm, buffer_imagen_1, longitud_archivos);

    fin = clock();

    free(buffer_imagen_1);
    free(buffer_Imagen_2);
    free(buffer_mask);

    return fin - inicio;
}

int llenar_buffer(char *archivo, unsigned char *buffer, int cantidad)
{
    FILE *file = fopen(archivo, "rb");
    fread(buffer, 270000, sizeof(unsigned char), file);
    return fclose(file);
}

int generar_BMP_salida(char *archivo, unsigned char *buffer, int cantidad)
{
    FILE *file = fopen(archivo, "w+");
    fwrite(buffer, 1, cantidad, file);
    return fclose(file);
}

/*
###################################################################################################################################
*/
int verificarNumeros(int largo, char *argv[], int num)
{
    int i;
    for (i = 0; i < largo; i++)
    {
        if (argv[num][i] < '0' || argv[num][i] > '9')
        {
            if (num == 4)
            {
                printf("El ancho no es un numero\n");
            }
            else
            {
                printf("El alto no es un numero\n");
            }
            return 1;
        }
    }
    return 0;
}

int verificarBMP_RGB(int largo, char *argv[], int num)
{
    if (largo < 5 || argv[num][largo - 4] != '.' || argv[num][largo - 3] != 'b' || argv[num][largo - 2] != 'm' || argv[num][largo - 1] != 'p')
    {   
        if (num == 3)
        {
            printf("La mascara no es un .bmp o .rgb\n");
        }
        else
        {
            printf("La imagen %d no es un .bmp o .rgb\n", num);
        }
        return 1;
    }
    return 0;
}

void buscar_en_carpeta(char *ancho, char *alto, char *imageFile1, char path[150])
{
    strcat(path, ancho);
    strcat(path, "x");
    strcat(path, alto);
    strcat(path, ".");
    strcat(path, imageFile1);
}

int verificarArchivo(char *image)
{
    FILE *file;
    if ((file = fopen(image, "r")) == NULL)
    {
        printf("No existe %s\n", image);
        return 1;
    }
    fclose(file);
    return 0;
}